# -*- coding: utf-8 -*-
__author__ = 'babache'

import binascii

flag = ""
f = open('flag_63c24d48595eae318c9a174f37ffb0f128758e5c16fea0ffebf12b0ba5f5b26a.txt', 'r')
for line in f:
	words = line.split(' ')
	for word in words:
		caractere = int('0b'+word, 2)
		flag += binascii.unhexlify('%x' % caractere)
f.close()
print flag