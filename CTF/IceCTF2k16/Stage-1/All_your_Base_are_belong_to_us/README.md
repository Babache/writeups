# All your Base are belong to us - 15pts

```
What a mess... we got a raw flag but now what do we do... flag.txt 
```

Contenu de flag.txt :
```
01001001 01100011 01100101 01000011 01010100 01000110 01111011 01100001 01101100 00110001 01011111
01101101 01111001 01011111 01100010 01100001 01110011 01100101 01110011 01011111 01100001 01110010
01100101 01011111 01111001 01101111 01110101 01110010 01110011 01011111 01100001 01101110 01100100
01011111 01100001 01101100 01101100 01011111 01111001 00110000 01110101 01110010 01011111 01100010
01100001 01110011 01100101 01110011 01011111 01100001 01110010 01100101 01011111 01101101 01101001
01101110 01100101 01111101
```

Le contenu du fichier est en binaire, convertisons le :

```python
# -*- coding: utf-8 -*-
__author__ = 'babache'

import binascii

flag = ""
f = open('flag_63c24d48595eae318c9a174f37ffb0f128758e5c16fea0ffebf12b0ba5f5b26a.txt', 'r')
for line in f:
	words = line.split(' ')
	for word in words:
		caractere = int('0b'+word, 2)
		flag += binascii.unhexlify('%x' % caractere)
f.close()
print flag
```

**Flag** : IceCTF{al1_my_bases_are_yours_and_all_y0ur_bases_are_mine}