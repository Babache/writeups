# Move Along - 30pts

```text
This site seems awfully suspicious, do you think you can figure out what they're hiding?
http://move-along.vuln.icec.tf/
```

Code source de la page :

```html
<!DOCTYPE html>
<html>
    <head>
        <title>IceCTF 2016 - Move Along</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
        <img src="move_along/nothing-to-see-here.jpg"></img>
    </body>
</html>

```

On vois qu'une image est affiché sur cette page et qu'elle se trouve dans un répertoire "move_along". Il est parait évident qu'il faut voir si en accèdenant au répertoire le contenu est affiché.

Allons sur sur http://move-along.vuln.icec.tf/move_along/ :
```
Index of /move_along/

../
0f76da769d67e021518f05b552406ff6/                  10-Aug-2016 19:07                   -
nothing-to-see-here.jpg                            10-Aug-2016 19:07               19453

```

Interresent car le contenu est affiché, chose à ne pas faire en production sauf si cela est voulu.

Etape suivante entrons dans le répertoire "0f76da769d67e021518f05b552406ff6"
```
Index of /move_along/0f76da769d67e021518f05b552406ff6/

../
secret.jpg                                         10-Aug-2016 19:07              104677
```

Ahah, un secret ?

Jackpot l'image contient le flag !

**Flag** : IceCTF{tH3_c4t_15_Ou7_oF_THe_b49}