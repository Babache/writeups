# Time traveler - 30pts

```text
I can assure you that the flag was on this website (http://time-traveler.icec.tf/) at some point in time. 
```

Il est clairement dis qu'à un instant "T" le flag était présent sur cette page. 
Il existe un site qui effectue des "screenshot" de page : https://archive.org/

On indique donc notre URL : http://time-traveler.icec.tf/
On s'apperçois qu'il y a une sauvegarde au 1 juin 2016 :
https://web.archive.org/web/20160601212948/http://time-traveler.icec.tf/

**Flag** : IceCTF{Th3y'11_n3v4r_f1|\|d_m4h_fl3g_1n_th3_p45t}