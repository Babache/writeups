## Description
Il faut envoyer en moins de 100 secondes, 50 bonnes réponses.

## Aperçu challenge

![Challenge View](https://gitlab.com/Babache/writeups/raw/master/CTF/Insomnihack2k17/Capscii/capture_challenge.png)

## Solution

Pour résoudre le challenge, il faut récupérer le code source de la page. 
Par la suite on ne garde que la partie qui nous intéresse, se trouvant dans la div.

![Challenge View](https://gitlab.com/Babache/writeups/raw/master/CTF/Insomnihack2k17/Capscii/capture_source_challenge.png)

Ce qui donne :

```html
FRP  YTM  Y J  HCN       PQD  TUW  QDN  XBP  <br>Z I  G N  H P  N     C   P      O  B    J    <br>LNH  CDT  MAD  LWQ  UKM  LCJ  ZBK  VOX  IIP  <br>T E    V    G  M R   T   T W  M      N  O W  <br>HGM  UNM    J  ACY       CYW  GGI  IZG  OMW  <br><br>
```

Afin de simplifier le traitement on remplace tous les caractères alphabétiques par un caractère unique, dans mon cas par un "#" ainsi que les sauts de ligne HTML "<br>" par un saut de ligne "\n".


```php
<?php

// ligne de correspondance
function convert($chiffre){
	switch($chiffre)
	{
		case "##  #  #  # ###":$number = 1;break;
		case "###  #####  ###";$number = 2;break;
		case "###  ####  ####":$number = 3;break;
		case "# ## ####  #  #":$number = 4;break;
		case "####  ###  ####":$number = 5;break;
		case "####  #### ####":$number = 6;break;
		case "###  #  #  #  #":$number = 7;break;
		case "#### ##### ####":$number = 8;break;
		case "#### ####  ####":$number = 9;break;
		case "#### ## ## ####":$number = 0;break;
		case "    # ### #    ":$number = "+";break;
		case "      ###      ":$number = "-";break;
		case "       #       ":$number = "*";break;
	}
	return $number;

}
// on nettoie pour prendre que ce qui nous intéresse
function cleanF($data){
	$data = substr($data,215);
	$data = substr($data,0,strpos($data,"<br><br>"));
	$data = str_replace('<br>',"\n",$data);
	$data = str_replace(array('A','Z','E','R','T','Y','U','I','O','P','Q','S','D','F','G','H','J','K','L','M','W','X','C','V','B','N'),'#',$data);
	return $data;
}

$ch = curl_init('http://capscii.insomni.hack/start');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE);
curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE);
// Pour une trace via Burpsuite par exemple
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
//curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8081');
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
$data = curl_exec($ch);
// réponses à faire
for($i=0;$i<=50;$i++)
{
	$liste = explode("\n", cleanF($data));
	$ligne1 = $liste[0];
	$ligne2 = $liste[1];
	$ligne3 = $liste[2];
	$ligne4 = $liste[3];
	$ligne5 = $liste[4];
	$chaine ="";
	for($start=0;$start<strlen($ligne1);$start++){
		$group = substr($ligne1,$start,3);
		$group .= substr($ligne2,$start,3);
		$group .= substr($ligne3,$start,3);
		$group .= substr($ligne4,$start,3);
		$group .= substr($ligne5,$start,3);
		$chaine .=convert($group);
		$start+=4; // Tous les 5 caractères
	}
	eval('$cpt = '.$chaine.';');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,"sol=".($cpt));
	$data = curl_exec ($ch);
}
```

On exécute et après nos 50 bonnes réponses :

````Flag
Congratz the flag is INS{O_o firSt_t1ime_U_seE_it o_O}
````



