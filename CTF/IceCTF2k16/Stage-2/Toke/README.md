# Toke - 45pts

```
I have a feeling they were pretty high when they made this website... 
```
http://toke.vuln.icec.tf/

On regarde un peu la console si des messages sont présents ?  des cookies ? Rien appriori.

Créons nous un compte si cela est possible. Le compte créé connectons-nous !

Même chose regardons nos cookies, un cookie apparait :

```
jwt_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYmFiYWNoZSIsImZsYWciOiJJY2VDVEZ7alc3X3QwSzNuc180UmVfbk9fcDE0Q0VfZk9SXzUzQ3JFN1N9In0.OXjzceQvDU_CH8kDZaurS5iVPtBiYBD9HdvFjkOlqsk
```

Cherchons sur internet si ce "jwt_token" est issue d'un framework, il est peut être résercible et/ou exploitable. 

On trouve le site facilement le site : https://jwt.io/ on y colle notre token 

HEADER:ALGORITHM & TOKEN TYPE
```
{
  "alg": "HS256",
  "typ": "JWT"
}
```

PAYLOAD:DATA
```
{
  "user": "babache",
  "flag": "IceCTF{jW7_t0K3ns_4Re_nO_p14CE_fOR_53CrE7S}"
}
```

VERIFY SIGNATURE
```
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
secret
) secret base64 encoded
```

**Flag** : IceCTF{jW7_t0K3ns_4Re_nO_p14CE_fOR_53CrE7S}