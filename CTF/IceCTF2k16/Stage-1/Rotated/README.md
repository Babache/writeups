# Rotated - 20pts

```
They went and ROTated the flag by 5 and then ROTated it by 8! The scoundrels! Anyway once they were done this was all that was left VprPGS{jnvg_bar_cyhf_1_vf_3?} 
```

La solution est dans le message, une rotation de 5 + une rotation de 8 = rotation de 13.

Firebug : ROT13 de VprPGS{jnvg_bar_cyhf_1_vf_3?} 

**Flag** : IceCTF{wait_one_plus_1_is_3?}