# Alien Message - 40pts

```
We found this suspicous image online and it looked like it had been planted there by an alien life form. Can you see if you can figure out what they're trying to tell us?
```

Une image avec un message écrit dans un langage inconnu. Une petite recherche sur internet sur "Alien message" me permet de trouver un alphabet : "Alien Alphabet".

Il suffit alors de traduire avec l'alphabet (la case ne semble pas pris en compte).

**Flag** : iceCTF{good_n3wz_3veryon3_1_l1k3_fU7ur4Ma_4nD_tH3ir_4MaZ1ng_3as7er_39g5}