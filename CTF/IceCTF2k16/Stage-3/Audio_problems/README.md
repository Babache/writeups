# Audio problems - 50pts

```
We intercepted this audio signal, it sounds like there could be something hidden in it. Can you take a look and see if you can find anything? 
```

Une 1ère écoute du fichier montre que le son ne donne rien de "clair". 

On peut essayer d'inverser le son, ralentir, accèlerer mais cela ne semble pas être la bonne approche. 

Une autre approche serait d'analyser les spectres de l'audio. L'outil http://spek.cc/ est très bien fait pour ça.

Voir : Spectogramme.png

**Flag** : IceCTF{y0u_b3t7Er_l15TeN_cL053ly}